--[[
KevinBot by Kevinkev

	Please use a colour based text editor to optimally manage settings: Notepad++ or Sublime come to mind.
	[Notepad++ download link]: http://download.tuxfamily.org/notepadplus/6.3/npp.6.3.Installer.exe
	--Configuration--

]]
function config()
    
    
    --AUTOMATIC UPDATES
    automaticUpdates = true -- Checks for updates every time the exe is run.
    stopAfterLoLUpdate = true -- Stops the bot when LoL is updated (BoL might not be)
    
    --[[ Account switching options ]]--
    bot_accountRotation = false
    --[[ Select the region to automate the accounts list
        ["na"] = "North America",
        ["euw"] = "Europe West",
        ["eun"] = "Europe Nordic/East",
        ["br"] = "Brazil (Brasil)",
        ["tr"] = "Turkey",
        ["ru"] = "Russia",
        ["pbe"] = "PBE",
        ["oc"] = "Oceanic",
        ["kr"] = "Korea",
        ["tw"] = "Taiwan",
        ["sea"] = "Singapore/Malaysia",
        ["vn"] = "Vietnam",
        ["las"] = "Latin America South",
        ["lan"] = "Latin America North",
        ["china-ionia-1"] = "China Ionia HN1",
    ]]--
    bot_region_select = "oc"--The region which the bot is botting on.
    --BOOSTS--
    bot_buyBoostAtLvl = 30  --The level at which boosts are bought. If you don't want to buy boosts put 31
    bot_boostDAYType = 1 --Only 1,3 or 7 day boosts are available for automatic purchasing
    --LEVEL OPTIONS
    bot_rotateAtLevel = 31  --Changes to a different account when this level is detected on the current
    --MISC.
    bot_accountsList = "accounts_list.txt" --Contains the user account info (username:password:summonerName[optional])
    bot_accountsCompleted = "accounts_completed.txt" --Puts details of completed accounts here
    bot_boostChecker = "accounts_boost.txt" --Marks an account if a boost has already been bought
    bot_current_working = "account_current.txt" --Local current file for the bot
    bot_consumeFromList = false --If you're using a shared list then you'll need this
    
    
    --Please set this path before configuring other options (Slashes must be like this "/")
	LOL_PATH = "D:/Riot Games/League of Legends/"
	usingGarena = false -- Set this to true if you're on or using garena
	--[[ UTILITIES ]]--
	--takes a screenshot at the end of each game -scoreboard (true/false)
	takeScreenshot = true

	--[[ AUTO LOGIN ]]--
	--This will allow you to bot automatically just through running the exe--
	--Use it only if you want to of course, purely a convinience thing.
	autoLogin = true
	userpassFile = "login.txt" --Change the file path if you want.
	

	
	--[[ CHAT ]]--
	--Appear offline in chat (with friends and such, this does not affect chat in-game)
	offlineMode = false
		
	--[[ OTHER MODES ]]--
    useKBOTtoresize = false -- If true, and the size of your League window is not viable then it will attempt to change it. Disabled due to LoL automatically adjusting now.
    makeCustoms = false --True if you want it to attempt to play custom games (patched way of getting ip/xp)
    externalExit = true --leave true if you have problems with league of legends autoclosing the game.
	acceptMatch = true --true if you want to accept the match
	hideWindows = false --Doesn't hide the windows if false
	changeHomeScreen = true -- Replaces the home screen with an image (so no clicking on links)
    backgroundURL = "http://localhost/" --Change this to any url you want to replace the homescreen. (Only if the previous is true)
	gamesBeforeReset = 0 -- The number of games it detects before restarting the bot (semi-automatic failsafe). 0 doesn't restart the bot.
	logErrors = true -- If you want to log errors
	PvPNameOverride = "PVP.net Client" --Change incase your default window is named differently (ctrl+alt+del to find out)
	emergencyRestarts = false --Enables the bot to automatically restart the league and the bot after 1 hour of no games.
    
	--[[ CHAMP SELECTION MODE ]]--
	--1 random, 2 single champ, 3 queue of champs
	mode = 3

	--[[ GAME MODE ]]--
	--COOP = 1, PVP = 2, Customs only = 3
	gameMode = 1
	--Difficulty if playing COOP ( 1 beginner, 2 intermediate )
	difficulty = 2

	--[[ CHAMPION CONFIG ]]--
	--PUT IN IN THIS FASHION: {"champName",MasteryPage,RunePage,Summoner1,Summoner2}
	--Note that everything is a digit/number except for the champion name.
	
	--[[ SUMMONERS IN ORDER ]]
	--Revive, Clarity, Garrison,heal,ghost,cleanse,smite,barrier,ignite,exhaust,clairvoyance,flash
	--	   1,		2,		  3,	4,	  5,     6,	   7,	   8,	  9,     10,		  11,	12
	--OR use names like : 
    --REVIVE, CLARITY, GARRISON, HEAL , GHOST, CLEANSE, SMITE, BARRIER, IGNITE, EXHAUST, CLAIRVOYANCE, FLASH
    --
    
	--Will pick this champ all the time if mode is 2.
	--If this champ is taken will choose backup.
	--If backup is taken it will random a champion.
	
	singleChamp = {"Ryze",	1,1,6,9}
	backupChamp = {"Amumu",	1,1,6,9}
	
	--Remove any names you like out of the queue. Insert as many names (in quotes) as you wish (seperated by commas)

	championQueue = {
	--Champion      M R S S
	{"Ryze",		1,1,6,9},
	{"Annie",		1,1,6,8},
	{"Sivir",		1,1,10,9},
	{"Alistar",		1,1,10,9},
	

	}


end


--Do not touch this or else it won't work
loadEncrypted("jr4752iQOuGLxLtYsPM6lPW2xIxfad4+3A5XNwo+k5Qs3V4k5UOJhNJIRN4KGWxp5Md86jxdtVaLA7LE2oGjk3JHN36wcwjdV+4Y7Tr3zXPuf57skmeq7umpHHIwtxEh9oHIPrrXDfvJZA6JuiHwTZfXIEJQAhE36J1ec+3fngIRp+YuWVH+R68Ijv/8axdnafrh0+pzLG9yBFUiQKmF2vNYLDqavKo/qQjswed7yH0scFe97dwnsdMQXOZ9Z9zmAIwYG65p87qP+2aWDlaJzNpYOVGdRloAvRzYUSPbdVSkEhFN+yMDs/J12OFreDdFLkSa5RHT1lc5FBZTNQItmvFhYuZL1mLbyzGK2EkfMMc48Xa5zwWTkJS9OHzhw13PRpkFDVJgLh4YgP/k6uan9INHKDPt1vHmv5bJ8RU1UWWz3YaVAyjOJ5sFaSefVFCFf9MudeUwBC9BuZFqa3ix03EzAHkrGOX3lENBG2H/FcvDywP6v9kXwdgr44cfoCUE5p2JzliypcexvuA5QUn0c8Q5XwMxvWdUw8/NwU9qqqbZdbgNRf6+P0ec1c64fsoclLSd2VaW0GwiwhbkapW9yCnFh13ghg0Ef3v3hZCa+iYsxLP5DoJFulR19OOuN3UkhuMXHaPdlI/yaIYTQ8FZaf87UfrRxgpjzYua4fIgMvPe7ZQNjFORBBe1vlestb4gVgHonL5XBAK9tf8cAKolgWg7MItt7GUEzYCOcxj6tO+V6EENbe6K5mu0IPtvqrzzfCgBA1UCbAcT9nci/sAN9QzrWMQMG0ExBYaZjGoxBmLU75Ull9ZTnULqJSRA3EK33SwwYm82vPupTzsqHhcXpLws+0lcW2ykWpQYenKT6GE34ob0sy7GbcE6w7waOwns/TnB7JvnP0ZNvkRGEAi6g0LWm5NlGwAWZzOCLumti0uU+0/4wUqy4uvNb/R/+BPUG39u03tN4ooce8mrqs6fXWPK3/2kfcKikC41Ty4Pv2a9u71bvWY1C0COq5FqKgPGheh+sXU2QtuiQyOXxJ4RELX3hXRvQCM1ttYIUc/MoO39J8ovrldn4ewcfrrkY7vd2cpV6lFO3RoRwEiYzHR067As/ag0Y5FvtwMuA8C1nyenmyMLxLlmgEnTl9JwsU9LSrZs4756MtZIP3AUPCxemgEPEEteDuqZaFyfCTwEQFHDUqY/DB7LVQkjImBykErae7M/2uGL4lq/0us/ffMJQCPxrf+Uck+umYrMKpaNcGh4ZPOS+m8vsfM8jiwiu7NWZF6S+Xs8inlrP//FunpEcjcnVTCO2X1XiASpRxJ/XMYEcTafWI4aPgBdoUkRX1NQWvkrQWVGjKWC3VzTJGniCljRwotY1Vf9nhRyqrMoUrXhXNbv2LlCGSUZiyVEX8AssEj/2sGvaremKlNUnGdWBUFnuhNwSKAHkxgRs9iqLpL1p/0GjehjdNKUSxABIrC3bYcvS7lDkZ0Q4k0qSBTLRmeoWcX9VZX/oB3s/KxTVu3/ZtZaJgq0YVO7ZHfd5BFyf5JhzMNG70AZ9tYkfqkrFzmMeRP5DlWO0xu7c8/pv5/h+IXJO4GMthjqhqDU4QH07wgZnCAqk+2jVXM9hoZIGBarjlbtxMtIsTL0M1C9Zo/s1voHYK6pNAOnWDpoBMWLwb/CJGaIHc5LGHOYX/7WEw==")
